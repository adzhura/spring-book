package com.otus.spring.book.repository;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import com.otus.spring.book.domain.Author;
import com.otus.spring.book.domain.Book;
import com.otus.spring.book.domain.Comment;
import com.otus.spring.book.domain.Genre;


@Configuration
public class TestingBeans
{
    @Bean
    @Scope("prototype")
    public Genre genreCreate()
    {
        return new Genre("Test genre");
    }

    @Bean
    @Scope("prototype")
    public Genre genreUpdate()
    {
        Genre genre = new Genre("Test genre !!!");

        genre.setId(Integer.MAX_VALUE);

        return genre;
    }

    @Bean
    @Scope("prototype")
    public Author authorCreate()
    {
        return new Author("William", "Shakespeare");
    }

    @Bean
    @Scope("prototype")
    public Author authorUpdate()
    {
        Author author = new Author("William", "Shakespeare !!!");

        author.setId(Integer.MAX_VALUE);

        return author;
    }

    @Bean
    @Scope("prototype")
    public Book bookCreate()
    {
        Book book = new Book();

        // book.setId(Integer.MAX_VALUE);
        book.setName("Test book");

        return book;
    }

    @Bean
    @Scope("prototype")
    public Book bookUpdate()
    {
        Book book = new Book();

        book.setId(Integer.MAX_VALUE);
        book.setName("Test book !!!");

        return book;
    }

    @Bean
    @Scope("prototype")
    public Comment commentCreate()
    {
        Comment comment = new Comment();

        // book.setId(Integer.MAX_VALUE);
        comment.setUserName("anonymous");
        comment.setText("Test comment");

        return comment;
    }

    @Bean
    @Scope("prototype")
    public Comment commentUpdate()
    {
        Comment comment = new Comment();

        comment.setId(Integer.MAX_VALUE);
        comment.setUserName("anonymous");
        comment.setText("Modified comment");

        return comment;
    }
}
