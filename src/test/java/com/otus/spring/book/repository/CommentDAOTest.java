package com.otus.spring.book.repository;


import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.assertj.core.api.Condition;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.otus.spring.book.domain.Book;
import com.otus.spring.book.domain.Comment;
import com.otus.spring.book.service.CommentService;


@RunWith(SpringRunner.class)
@SpringBootTest
public class CommentDAOTest extends AbstractDAOTest
{
    @Autowired
    private CommentRepository commentDao;

    @Autowired
    private CommentService commentService;

    @Autowired
    private BookRepository bookDao;

    @Autowired
    private Comment commentCreate;
    @Autowired
    private Comment commentUpdate;

    private Optional<Book> book;

    @Before
    public void init()
    {
        book = bookDao.findById(1);
    }

    @Test
    public void findAll()
    {
        testFindAll(commentDao, 12);
    }

    @Test
    public void findById()
    {
        testFindById(commentDao, 1);
    }

    @Test
    public void findByBookId()
    {
        assertThat(commentService.getComments(1)).hasSize(3);
    }

    @Test
    public void findByUserName()
    {
        assertThat(commentService.getComments("user1")).hasSize(4);
    }

    @Test
    public void create()
    {
        commentCreate.setBook(book.get());
        testCreate(commentDao, commentCreate);

        String userName = "anonymous";
        String text = "another text";
        Comment comment = commentService.createComment(book.get().getId(),
            userName, text);
        assertThat(comment).isNotNull();
        assertThat(comment).is(new Condition<Comment>(
            c -> userName.equals(c.getUserName()) && text.equals(c.getText()),
            ""));

        commentDao.delete(comment);
    }

    @Test
    public void update()
    {
        commentCreate.setBook(book.get());
        commentUpdate.setBook(book.get());
        testUpdate(commentDao, commentCreate, commentUpdate);
    }

    @Test
    public void delete()
    {
        commentCreate.setBook(book.get());
        testDelete(commentDao, commentCreate);
    }
}
