package com.otus.spring.book.repository;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.otus.spring.book.domain.Author;


@RunWith(SpringRunner.class)
@SpringBootTest
public class AuthorDAOTest extends AbstractDAOTest
{
    @Autowired
    private AuthorRepository dao;

    @Autowired
    private Author authorCreate;
    @Autowired
    private Author authorUpdate;

    @Test
    public void findAll()
    {
        testFindAll(dao, 2);
    }

    @Test
    public void findById()
    {
        testFindById(dao, 1);
    }

    @Test
    public void findByName()
    {
        testFindByName(dao, "Иван", t -> "Иван".equals(t.getFirstName()));
        testFindByName(dao, "Иванов", t -> "Иванов".equals(t.getLastName()));
    }

    @Test
    public void create()
    {
        testCreate(dao, authorCreate);
    }

    @Test
    public void update()
    {
        testUpdate(dao, authorCreate, authorUpdate);
    }

    @Test
    public void delete()
    {
        testDelete(dao, authorCreate);
    }
}
