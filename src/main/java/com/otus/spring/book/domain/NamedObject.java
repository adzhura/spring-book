package com.otus.spring.book.domain;


public interface NamedObject<T> extends HasId<T>
{
    String getName();
}
