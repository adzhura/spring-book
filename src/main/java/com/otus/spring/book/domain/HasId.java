package com.otus.spring.book.domain;


public interface HasId<T>
{
    T getId();

    void setId(T id);
}
