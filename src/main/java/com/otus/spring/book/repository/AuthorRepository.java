package com.otus.spring.book.repository;


import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;

import com.otus.spring.book.domain.Author;


public interface AuthorRepository
        extends JpaRepository<Author, Integer>, NamedObjectRepository<Author>
{
    Collection<Author> findByFirstNameOrLastName(String firstName,
        String lastName);

    @Override
    default Collection<Author> findByName(String name)
    {
        return findByFirstNameOrLastName(name, name);
    }
}
