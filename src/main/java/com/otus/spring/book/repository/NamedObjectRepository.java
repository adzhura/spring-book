package com.otus.spring.book.repository;


import java.util.Collection;


public interface NamedObjectRepository<T>
{
    Collection<T> findByName(String name);
}
