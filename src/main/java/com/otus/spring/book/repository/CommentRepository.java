package com.otus.spring.book.repository;


import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;

import com.otus.spring.book.domain.Book;
import com.otus.spring.book.domain.Comment;


public interface CommentRepository extends JpaRepository<Comment, Integer>
{
    Collection<Comment> findByUserName(String userName);

    Collection<Comment> findByBook(Book book);
}
