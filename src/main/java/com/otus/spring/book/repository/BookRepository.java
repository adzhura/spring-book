package com.otus.spring.book.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.otus.spring.book.domain.Book;


public interface BookRepository
        extends JpaRepository<Book, Integer>, NamedObjectRepository<Book>
{

}
