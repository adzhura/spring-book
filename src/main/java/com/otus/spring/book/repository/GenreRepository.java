package com.otus.spring.book.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.otus.spring.book.domain.Genre;


public interface GenreRepository
        extends JpaRepository<Genre, Integer>, NamedObjectRepository<Genre>
{

}
