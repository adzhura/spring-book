package com.otus.spring.book.dto;


import com.otus.spring.book.domain.Comment;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentDto
{
    private Integer id;

    private String userName;

    private String text;

    public static CommentDto toDto(Comment comment)
    {
        return new CommentDto(comment.getId(), comment.getUserName(),
            comment.getText());
    }
}
