package com.otus.spring.book;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class BookApp
{
    public static void main(String[] args) throws Exception
    {
        SpringApplication.run(BookApp.class, args);
    }
}
